﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{


    //public SceneManager sceneMng;
    // Use this for initialization
    void Start()
    {


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject o = other.gameObject;
        if (o.tag == "PlayerActor")
        {
            queEndLevel();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void queEndLevel()
    {
        ScoreKeeper.S.keepTime = false;
        SceneManager.LoadScene("ScoreEntryScene");
    }
}
