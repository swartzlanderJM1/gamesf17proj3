﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrintScores : MonoBehaviour {

    public Text HighScoreNameText;
    public Text HighScoreValText;

    // Use this for initialization
    void Start()
    {

        // Find maximum index to search
        int max = Mathf.Min(ScoreKeeper.S.getScoreListCount(), 10);

        string nameList = "";
        string valList = "";
        for (int i = 1; i <= max; i++)
        {
            nameList += i.ToString() + ":    ";
            nameList += ScoreKeeper.S.getScoreListName(i);
            valList += ScoreKeeper.S.getScoreListVal(i);

            if (i != max)
            {
                nameList += '\n';
                valList += '\n';
            }
        }
        HighScoreNameText.text = nameList;
        HighScoreValText.text = valList;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
