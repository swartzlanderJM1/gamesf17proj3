﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour {

    private PlayerShoot ps;
	// Use this for initialization
	void Start () {
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject o = other.gameObject;
        if (o.tag == "PlayerActor")
        {
            ps = o.GetComponent<PlayerShoot>();
            o.GetComponentInChildren<AmmoCounter>().updateCount(7);
            ps.ammoCount = 7;
            Destroy(this.gameObject.transform.parent.gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
