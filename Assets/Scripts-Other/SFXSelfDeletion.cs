﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXSelfDeletion : MonoBehaviour {

    private float currTime = 0;
    private const float lifetime = 1f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        currTime += Time.deltaTime;
        if (currTime >= lifetime)
        {
            Destroy(gameObject); // Destroy self
        }
    }
}
