﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level2Start : MonoBehaviour {


    //public SceneManager sceneMng;
	// Use this for initialization
	void Start () {
        

	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject o = other.gameObject;
        if (o.tag == "PlayerActor")
        {
            queLevel2();
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
    private void queLevel2()
    {
        ScoreKeeper.S.keepTime = true;
        SceneManager.LoadScene("SecondLevel");
    }
}
