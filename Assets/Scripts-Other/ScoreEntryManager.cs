﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreEntryManager : MonoBehaviour
{

    private string saveEntryName = "";
    public Text entryText;
    public Text valText;

    // Use this for initialization
    void Start()
    {
        string rawVal = ScoreKeeper.S.currScore.ToString();

        if (ScoreKeeper.S.currScore != 0f)
        {
            rawVal.Remove(rawVal.IndexOf('.'));

            valText.text = rawVal;
        }
        else
        {
            valText.text = float.NaN.ToString();
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            GoToGameOver();
        }
    }

    public void GoToGameOver()
    {
        saveEntryName = entryText.text;

        // Save score if player entered a name
        if (saveEntryName != "")
        {
            ScoreKeeper.S.saveCurrScore(saveEntryName);
        }
        SceneManager.LoadScene("ResultScreen");
    }
}
