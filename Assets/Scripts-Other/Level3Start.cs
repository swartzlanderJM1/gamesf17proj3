﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level3Start : MonoBehaviour
{


    //public SceneManager sceneMng;
    // Use this for initialization
    void Start()
    {


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject o = other.gameObject;
        if (o.tag == "PlayerActor")
        {
            queLevel3();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void queLevel3()
    {
        ScoreKeeper.S.keepTime = true;
        SceneManager.LoadScene("ThirdLevel");
    }
}
