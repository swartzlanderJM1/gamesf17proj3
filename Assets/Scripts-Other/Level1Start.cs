﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level1Start : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Space))
        {
            queLevel1();
        }
	}

    private void queLevel1()
    {
        ScoreKeeper.S.keepTime = true;
        SceneManager.LoadScene("FirstLevel");
    }
}
