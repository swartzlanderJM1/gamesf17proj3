﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour {

    Vector3 startPos;

	// Use this for initialization
	void Start () {
        startPos = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void React()
    {
        this.transform.position = startPos;

        // Give the player an extra bullet to work with
        this.GetComponent<PlayerShoot>().ammoCount++;
        GetComponentInChildren<AmmoCounter>().updateCount(this.GetComponent<PlayerShoot>().ammoCount);
    }
}
