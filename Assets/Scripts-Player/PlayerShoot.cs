﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShoot : MonoBehaviour
{

    private GameObject shotPrefab;
    private Vector3 shotOriginOffset;
    private float timeSinceLastShot = 0f;
    private float ShotCoolDown = 0.7f;
    private float clickTimer = 0f;
    public int ammoCount = 7;
    AmmoCounter amCountMeth;

    GameObject shotSFX;
    GameObject noAmmoSFX;
    GameObject reloadSFX;

    // Use this for initialization
    void Start()
    {
        shotOriginOffset = new Vector3(0, 3.02f * transform.localScale.y * 0.5f, 0);
        shotPrefab = (GameObject)Resources.Load("ShotLine");
        shotSFX = (GameObject)Resources.Load("PlayerShotSFX");
        noAmmoSFX = (GameObject)Resources.Load("OutOfAmmoSFX"); 
        reloadSFX = (GameObject)Resources.Load("ReloadSFX");
        amCountMeth = transform.GetComponentInChildren<AmmoCounter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Fire1") == 1 && timeSinceLastShot >= ShotCoolDown && ammoCount > 0)
        {
            makeShot();
            timeSinceLastShot = 0f;
            ammoCount--;
            amCountMeth.updateCount(ammoCount);
        }
        else if (Input.GetAxis("Fire1") == 1 && ammoCount <= 0 && clickTimer >= 0.15f)
        {
            Instantiate(noAmmoSFX);
            clickTimer = 0;
        }

        
        timeSinceLastShot += Time.deltaTime;
        clickTimer += Time.deltaTime;
    }

    private void makeShot()
    {
        Vector3 shotOrigin = this.transform.position + transform.TransformDirection(shotOriginOffset);
        RaycastHit2D[] enemyHit = Physics2D.RaycastAll(shotOrigin, this.transform.up);
        RaycastHit[] wallHit = Physics.RaycastAll(shotOrigin, this.transform.up);

        int closestWallIndex = GetClosestWall(wallHit);
        int enemyIndex = GetClosestEnemy(enemyHit);
        if (closestWallIndex != -1 && enemyIndex != -1)
        {
            // Case 1: if shot hits both an enemy and a wall and the wall is closer
            if (wallHit[closestWallIndex].distance <= enemyHit[enemyIndex].distance)
            {
                HitWall(wallHit[closestWallIndex]);
            }
            // Case 2: if shot hits both an enemy and a wall and the enemy is closer
            else
            {
                HitEnemy(enemyHit[enemyIndex]);
            }
        }
        //Case 3: Only a wall is hit
        else if (closestWallIndex != -1)
        {
            HitWall(wallHit[closestWallIndex]);
        }
        // Case 4: only an enemy is hit
        else if (enemyIndex != -1)
        {
            HitEnemy(enemyHit[enemyIndex]);
        }
        // Case 5: nothing is hit
    }
    
    private int GetClosestWall(RaycastHit[] hitList)
    {
        float closestDist = float.PositiveInfinity;

        int currClosestPoint = -1;

        for (int i = 0; i < hitList.Length; i++)
        {
            if (hitList[i].collider != null && hitList[i].collider.tag == "Wall")
            {
                float distToPoint = hitList[i].distance;
                if (distToPoint < closestDist)
                {
                    closestDist = distToPoint;
                    currClosestPoint = i;
                }
            }
        }

        return currClosestPoint;
    }

    private int GetClosestEnemy(RaycastHit2D[] hitList)
    {
        float closestDist = float.PositiveInfinity;

        int currClosestPoint = -1;

        for (int i = 0; i < hitList.Length; i++)
        {
            if (hitList[i].collider != null && hitList[i].collider.tag == "Enemy")
            {
                float distToPoint = hitList[i].distance;
                if (distToPoint < closestDist)
                {
                    closestDist = distToPoint;
                    currClosestPoint = i;
                }
            }
        }

        return currClosestPoint;
    }

    private void HitEnemy(RaycastHit2D hit)
    {
        DrawShotToPoint(hit.point);
        hit.collider.GetComponent<EnemyDeath>().Kill();
        Instantiate(shotSFX);
        StartCoroutine("ReloadSound");
    }

    private void HitWall(RaycastHit hit)
    {
        DrawShotToPoint(hit.point);
        Instantiate(shotSFX);
        StartCoroutine("ReloadSound");
    }

    private void DrawShotToPoint(Vector3 point)
    {
        GameObject newShot = Instantiate(shotPrefab);
        newShot.transform.position = this.transform.position + transform.TransformDirection(shotOriginOffset);
        Vector3[] linePoints = new Vector3[] { Vector3.zero, point - this.transform.position - transform.TransformDirection(shotOriginOffset)};
        linePoints[1].z = 0;
        newShot.GetComponent<LineRenderer>().SetPositions(linePoints);
    }

    private IEnumerator ReloadSound()
    {
        yield return new WaitForSeconds(0.2f);
        Instantiate(reloadSFX);
        yield return null;
    }
}
