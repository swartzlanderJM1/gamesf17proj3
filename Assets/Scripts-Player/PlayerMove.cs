﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

    private const float moveSpeed = 5;
    private const float wallProxLimit = 0.6f;

    Vector3 moveDelta;
    Vector3 raycastOrigin;
    Vector3 raycastOffset;

    //GameObject debugIndic;

    // Use this for initialization
    void Start()
    {
        //raycastOffset = new Vector3(-0.3f * this.transform.localScale.x * 2.06f, 0, 0);
        raycastOffset = new Vector3(0, 0, 0);
        //debugIndic = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //debugIndic.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        raycastOrigin = this.transform.position + transform.TransformDirection(raycastOffset);
        //debugIndic.transform.position = raycastOrigin;
        // Slow down player when rubbing against a wall (help prevent some movement through walls)
        MoveKeys(1);


    }

    private void LateUpdate()
    {
        LookAtCursor(); // Look at cursor once movement is complete
    }

    void LookAtCursor()
    {
        Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;
        cursorPos.z = this.transform.position.z;
        transform.up = cursorPos;
        transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
    }

    void MoveKeys(float moveScale)
    {
        moveDelta = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0) * moveScale * moveSpeed * Time.deltaTime;


        RaycastHit[] hitListX = Physics.RaycastAll(raycastOrigin, new Vector3(moveDelta.x, 0, 0));
        bool thereIsAWallX = false;
        foreach (RaycastHit i in hitListX)
        {
            if (i.collider != null && i.collider.tag == "Wall" && i.distance < wallProxLimit)
            {
                thereIsAWallX = true;
            }
        }

        if(thereIsAWallX)
        {
            moveDelta.x = 0;
        }

        bool thereIsAWallY = false;
        RaycastHit[] hitListY = Physics.RaycastAll(raycastOrigin, new Vector3(0, moveDelta.y, 0));
        foreach (RaycastHit i in hitListY)
        {
            if (i.collider != null && i.collider.tag == "Wall" && i.distance < wallProxLimit)
            {
                thereIsAWallY = true;
            }
        }

        if (thereIsAWallY)
        {
            moveDelta.y = 0;
        }

        this.transform.position += moveDelta;
    }


}
