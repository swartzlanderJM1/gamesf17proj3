﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackObject : MonoBehaviour {

    // Set this in the editor
    public GameObject toTrack;

	// Use this for initialization
	void Start () {
		
	}
	
	// Move this object in the 2D plane to match the XY coordinates of the tracked object
	void LateUpdate () {
        transform.position = new Vector3 (toTrack.transform.position.x, toTrack.transform.position.y, transform.position.z);
	}
}
