﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

// A singleton built to load and store name-value pairs via a text file
// Scores are floating point values based off time

public class ScoreKeeper : MonoBehaviour {

    public static ScoreKeeper S;

    private List<Score> scoreList;
    private TextAsset scoreListFile;
    private AudioSource BGM;
    public float currScore { get; private set; }

    // Turn the scorekeeping on or off
    public bool keepTime = false;

    private class Score : IComparable<Score>
    {
        public string name { get; private set; }
        public float val { get; private set; }

        public Score(string initName, float initVal)
        {
            name = initName;
            val = initVal;
        }

        public int CompareTo(Score other)
        {
            return val.CompareTo(other.val);
        }
    }

    void Awake()
    {
        if (S == null)
        {
            S = this;

            // Set up score list; default to mode A
            S.scoreList = new List<Score>();
            S.scoreListFile = Resources.Load<TextAsset>("ScoreListA");
            S.loadScores();
            S.currScore = 0;

            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(S);
            return;
        }

        Destroy(gameObject); // Destroy objects
    }

    // Use this for initialization
    void Start () {
        BGM = this.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (keepTime)
        {
            currScore += Time.deltaTime;
        }
	}

    private void loadScores()
    {
        if (S.scoreListFile.text == "")
        {
            return;
        }


        string rawString = S.scoreListFile.text;
        string[] scorePairs = rawString.Split('\n'); // Get strings in format: name1,score1\nname2,score2\n...

        List<Score> scoreListTemp = new List<Score>();

        // Insert each score to a temporary list

        foreach (string i in scorePairs)
        {
            if (i == "")
            {
                break;
            }
            string[] stringHalves = i.Split(','); // Split name-value pair
            string namePart = stringHalves[0];
            float valPart;
            if (!(float.TryParse(stringHalves[1], out valPart)))
            {
                throw new System.InvalidCastException("Failed to cast value part of score");
            }
            scoreListTemp.Add(new Score(namePart, valPart));
        }

        // Copy the newly-built list to the current score list

        S.scoreList.Clear();
        S.scoreList.AddRange(scoreListTemp);
        S.scoreList.Sort();
    }

    public void saveCurrScore(string name)
    {
        // Create the new score and add it to the current list
        Score newEntry = new Score(name, currScore);
        S.scoreList.Add(newEntry);
        S.scoreList.Sort();

        // Begin with empty string
        string saveString = "";

        // Append each score
        foreach (Score i in S.scoreList)
        {
            saveString += i.name + ',' + i.val.ToString() + '\n';
        }

        // Write out the score list to the file
        StreamWriter fileOut;
        // Choose file based on mode
        fileOut = new StreamWriter("Assets/Resources/ScoreListA.txt");

        fileOut.Write(saveString);
        fileOut.Close();
    }

    public void resetCurrScore()
    {
        S.currScore = 0;
    }

    public int getScoreListCount()
    {
        return S.scoreList.Count;
    }

    // Get first, second, etc. score value (1-indexed)
    public float getScoreListVal(int place)
    {
        //int i = S.scoreList.Count - place; // Put index in terms of list sorted low-to-high
        int i = place - 1; // Put index in terms of list sorted high-to-low
        return S.scoreList[i].val;
    }

    // Get first, second, etc. score name (1-indexed)
    public string getScoreListName(int place)
    {
        //int i = S.scoreList.Count - place; // Put index in terms of list sorted low-to-high
        int i = place - 1; // Put index in terms of list sorted high-to-low
        return S.scoreList[i].name;
    }

    // Starts background music (placed here to keep playing between scenes)
    public void startBGM()
    {
        BGM.Play();
    }
}
