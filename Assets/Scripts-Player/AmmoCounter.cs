﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoCounter : MonoBehaviour {

    TextMesh counter;

	// Use this for initialization
	void Start () {
        counter = this.GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void updateCount(int count)
    {
        string newCount = "Ammo\n";

        for (int i = 0; i < count; i++)
        {
            newCount += "- ";
        }

        if (count == 0)
        {
            newCount += "EMPTY!";
        }

        counter.text = newCount;
    }
}
