﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{

    GameObject bulletPrefab;
    GameObject shotSFX;
    float xOffset, yOffset;

    // Use this for initialization
    void Start()
    {
        bulletPrefab = (GameObject)Resources.Load("EnemyBullet");
        shotSFX = (GameObject)Resources.Load("EnemyShotSFX");
        xOffset = transform.localScale.x * 1.2f * 0.15f;
        yOffset = transform.localScale.y * 1.2f * 0.7f;
    }

    // Update is called once per frame
    void Update()
    {
        // For debug
        if (Input.GetKeyDown(KeyCode.Y))
        {
            ShootOnce();
        }
    }

    public void ShootOnce()
    {
        Vector3 bulletPos = new Vector3(xOffset, yOffset, 0);
        bulletPos = transform.TransformPoint(bulletPos);
        GameObject newBullet = Instantiate(bulletPrefab);
        newBullet.transform.position = bulletPos;
        newBullet.transform.rotation = this.transform.rotation;
        Instantiate(shotSFX);
    }
}
