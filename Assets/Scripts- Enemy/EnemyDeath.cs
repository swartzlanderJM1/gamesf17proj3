﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{

    EnemyWaypointPatrol moveScript;
    GameObject visCone;
    SpriteRenderer enemySprite;

    bool fadeOut = false;

    // Use this for initialization
    void Start()
    {
        moveScript = GetComponent<EnemyWaypointPatrol>();
        visCone = this.transform.GetChild(0).gameObject;
        enemySprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeOut)
        {
            Color fadeColor = enemySprite.color;
            fadeColor.a -= Time.deltaTime * 1;
            if (fadeColor.a <= 0.01f)
            {
                Destroy(this.gameObject);
            }
            enemySprite.color = fadeColor;
        }
    }

    public void Kill()
    {
        moveScript.enabled = false;
        visCone.SetActive(false);
        enemySprite.color = Color.red;
        fadeOut = true;
    }
}
