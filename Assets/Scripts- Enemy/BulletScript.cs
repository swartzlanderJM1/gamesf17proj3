﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    // For enemy bullets


    const float speed = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, this.transform.up, speed * Time.deltaTime);
        if (hit.collider != null && hit.collider.tag == "PlayerActor")
        {
            hit.collider.gameObject.GetComponent<PlayerHit>().React();
            Destroy(this.gameObject);
        }
        this.transform.position += this.transform.up * speed * Time.deltaTime;
	}

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
