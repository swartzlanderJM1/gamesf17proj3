﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: FOV = field of vision




public class VisionOcclude : MonoBehaviour
{

    float maxDist;
    float startLocalScaleY;
    float startScaleY;
    float startScaleX;

    Vector3 leftAngle;
    Vector3 rightAngle;

    // Use this for initialization
    void Start()
    {
        startLocalScaleY = transform.localScale.y;
        startScaleY = startLocalScaleY * transform.parent.localScale.y;
        startScaleX = transform.localScale.x * transform.parent.localScale.x;
        maxDist = 3.93f * startScaleY;

        // Toward the top-left corner
        leftAngle = new Vector2(-1.04f * startScaleX, 3.93f * startScaleY);
        leftAngle.Normalize();

        // Toward the top-right corner
        rightAngle = new Vector2(1.04f * startScaleX, 3.93f * startScaleY);
        rightAngle.Normalize();
    }

    // Update is called once per frame
    void Update()
    {

        // Check whether any rays hit
        List<RaycastHit> occlude = new List<RaycastHit>();
        RaycastHit midHit;
        RaycastHit leftHit;
        RaycastHit rightHit;

        Physics.Raycast(transform.position, transform.up, out midHit, maxDist, 1);
        occlude.Add(midHit);
        Physics.Raycast(transform.position, transform.TransformDirection(leftAngle), out leftHit, maxDist, 1);
        occlude.Add(leftHit);
        Physics.Raycast(transform.position, transform.TransformDirection(rightAngle), out rightHit, maxDist, 1);
        occlude.Add(rightHit);

        int closestPoint = getClosest(occlude);

        // If an object is blocking view
        if (closestPoint >= 0)
        {
            // Get the relative distance to that occluding point
            float dist = occlude[closestPoint].distance;
            // Shrink the FOV until it would not intersect that point
            transform.localScale = new Vector3(transform.localScale.x, startLocalScaleY * dist / maxDist, transform.localScale.z);
        }
        // If there was no blocking object
        else
        {
            // Use the initial FOV scale
            transform.localScale = new Vector3(transform.localScale.x, startLocalScaleY, transform.localScale.z);
        }

    }

    // Go through the list of raycast reults and find the point with the least distance
    // Reutrns the index of the raycast with that closest point
    // If no point was hit for any ray, return -1
    private int getClosest(List<RaycastHit> points)
    {
        float closestDist = float.PositiveInfinity;
        int currClosestPoint = -1;

        for (int i = 0; i < points.Count; i++)
        {
            if (points[i].collider != null && points[i].collider.tag != "PlayerActor")
            {
                float distToPoint = points[i].distance;
                if (distToPoint < closestDist)
                {
                    closestDist = distToPoint;
                    currClosestPoint = i;
                }
            }
        }

        return currClosestPoint;
    }
}
