﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour
{

    float visTimer = 0;
    float shotTimer = 0;
    const float visionTimeout = 2f;
    EnemyWaypointPatrol moveScript;
    EnemyShoot shotScript;

    int playerHitboxesSeen = 0;
    SpriteRenderer FOV_Sprite;

    Color idleColor = new Color(0,255,50);
    Color seenColor = new Color(255, 255, 0);

    public enum Vision_State { IDLE, SEEN };
    private Vision_State currState = Vision_State.IDLE;

    // Use this for initialization
    void Start()
    {
        FOV_Sprite = GetComponent<SpriteRenderer>();
        FOVStateChange(Vision_State.IDLE);
        moveScript = this.transform.parent.GetComponent<EnemyWaypointPatrol>();
        shotScript = this.transform.parent.GetComponent<EnemyShoot>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerHitboxesSeen > 0)
        {
            FOVStateChange(Vision_State.SEEN);
            visTimer = visionTimeout;
            moveScript.TrackPlayerWaypoint();
            if (shotTimer < 0)
            {
                shotScript.ShootOnce();
                shotTimer = 1f;
            }
        }
        else if (visTimer > 0)
        {
            FOVStateChange(Vision_State.SEEN);
        }
        else
        {
            FOVStateChange(Vision_State.IDLE);
        }

        visTimer -= Time.deltaTime;
        shotTimer -= Time.deltaTime;
    }

    private void FOVStateChange(Vision_State state)
    {
        currState = state;

        if (state == Vision_State.IDLE)
        {
            FOV_Sprite.color = idleColor;
        }
        else if (state == Vision_State.SEEN)
        {
            FOV_Sprite.color = seenColor;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerActor")
        {
            playerHitboxesSeen++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerActor")
        {
            playerHitboxesSeen--;
        }
    }

    public Vision_State GetVisionState()
    {
        return currState;
    }
}
