﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaypointPatrol : MonoBehaviour
{

    const float speed = 2f;

    public List<GameObject> waypoints = null;
    public GameObject player;

    EnemyVision visionStatus;

    int currPoint = 0;
    int patrolDirection = 1;
    bool skipMove = false;

    // Use this for initialization
    void Start()
    {

        visionStatus = gameObject.GetComponentInChildren<EnemyVision>();

        // Find the closest waypoint to this object and set it as the current waypoint
        currPoint = getClosestWaypoint(this.gameObject);

        // Move to that closest point and start patrolling to the next
        this.transform.position = new Vector3(waypoints[currPoint].transform.position.x, waypoints[currPoint].transform.position.y, this.transform.position.z);
        IncrementWaypoint();

    }

    // Update is called once per frame
    void Update()
    {

        // Check whether the waypoint has been reached
        float distToPoint = Get2DSqrDist(waypoints[currPoint].transform.position,this.transform.position);
        if (distToPoint < 0.1f) 
        {
            IncrementWaypoint();
        }

        // Rotate towards the player if seen, else towards next waypoint
        if (visionStatus.GetVisionState() == EnemyVision.Vision_State.IDLE)
        {
            this.transform.rotation = RotLerpTowardsObject2D(waypoints[currPoint]);
        }
        else
        {
            this.transform.rotation = RotLerpTowardsObject2D(player);
        }

        // Will not move if the player is currently seen
        if (skipMove == false)
        {
            this.transform.position += DirTowardsObject2D(waypoints[currPoint]) * Time.deltaTime * speed;
        }
        skipMove = false;
    }

    void IncrementWaypoint()
    {
        // When end of patrol line is reached, turn around
        if ((patrolDirection == -1 && currPoint <= 0) || (patrolDirection == 1 && currPoint >= (waypoints.Count - 1)))
        {
            patrolDirection *= -1;
        }

        currPoint += patrolDirection;
    }

    Vector3 DirTowardsObject2D(GameObject target)
    {
        Vector3 result = target.transform.position - this.transform.position;
        result.z = 0;
        result.Normalize();
        return result;
    }

    float Get2DSqrDist(Vector3 first, Vector3 second)
    {
        float deltaY = (first.y - second.y);
        float deltaX = (first.x - second.x);
        return deltaY * deltaY + deltaX * deltaX;
    }

    Quaternion RotLerpTowardsObject2D(GameObject target)
    {
        Vector3 dirToTarget = DirTowardsObject2D(target);
        Quaternion towardsPlayer = Quaternion.Euler(0, 0, Mathf.Atan2(dirToTarget.y, dirToTarget.x) * 180 / Mathf.PI - 90);
        return Quaternion.Slerp(this.transform.rotation, towardsPlayer, Time.deltaTime * 5);
    }

    // Used by EnemyVision to start moving towards player along waypoint path
    public void TrackPlayerWaypoint()
    {
        // Get the waypoint closest to the player's position
        int targetPoint = getClosestWaypoint(player);

        // If we're headed to that point already, do nothing
        if (targetPoint == currPoint)
        {
            skipMove = true;
            return;
        }

        // Find which way to start incrementing
        // Which gets a result closer to the target point: adding 1 or -1 to the current waypoint?
        int posInc = Mathf.Abs(targetPoint - (1 + currPoint));
        int negInc = Mathf.Abs(targetPoint - (-1 + currPoint));
        // Only alter the current waypoint if not headed the right way
        if (posInc < negInc && patrolDirection != 1)
        {
            patrolDirection = 1;
            currPoint += patrolDirection;
        }
        else if (negInc < posInc  && patrolDirection != -11)
        {
            patrolDirection = -1;
            currPoint += patrolDirection;
        }
    }

    private int getClosestWaypoint(GameObject target)
    {
        float closestDist = float.PositiveInfinity;
        int currClosestPoint = -1;

        for (int i = 0; i < waypoints.Count; i++)
        {
            float distToPoint = Get2DSqrDist(waypoints[i].transform.position, target.transform.position);
            if (distToPoint < closestDist)
            {
                closestDist = distToPoint;
                currClosestPoint = i;
            }
        }

        return currClosestPoint;
    }
}